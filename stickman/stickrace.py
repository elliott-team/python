#    Stickman a game
#    Copyleft (C) 2023  Meow Purr
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


from tkinter import *
import random
import time


class Coords:
    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

def whithin_x(co1, co2):
    if co1.x1 > co2.x1 and co1.x1 < co2.x2:
        return True
    elif co1.x1 > co2.x1 and co1.x2 < co2.x2:
        return True
    elif co2.x1 > co1.x1 and co2.x1 < co1.x1:
        return True
    elif co2.x2 > co1.x1 and co2.x2 < co1.x2:
        return True
    else:
        return False

def whithin_y(co1, co2):
    if (co1.y1 > co2.y1 and co1.y1 < co2.y2) \
            or (co1.y2 > co2.y2 and co1.y2) \
            or (co2.y1 > co1.y1 and co2.y1 < co1.y1)\
             or (co2.y2 > co1.y1 and co2.y2 < co1.y2):
        return True
    else:
        return False

def collided_left(co1, co2):
    if whithin_y(co1, co2):
        if co1.x1 <= co2.x2 and co1.x1 >= co2.x1:
            return True
    return False

def collided_right(co1, co2):
    if whithin_y(co1, co2):
        if co1.x2 >= co2.x1 and co1.x2 <= co2.x2:
            return True
        return False


def collided_top(co1, co2):
    if whithin_x(co1, co2):
        if co1.y1 <= co2.y2 and co1.y1 <= co2.y1:
            return True
        else:
            return False

    # is collided underneath?


def collided_botton(y, co1, co2):  # define funticion to know weather collided underneath
    if whithin_y(co1, co2):
        if co1.y2 + y >= co2.y1 and co1.y2 + y <= co2.y2:
            return True  # if colding state
        else:
            return False  # otherwise state


# Add the objects
class Sprite:
    def __init__(self, game):
        self.game = game
        self.endgame = False  # Off-course it`s not the endgame yet (the game just started)
        self.coordinates = None  # there are no coordinates yet

    def move(self):
        pass  # does nothing

    def coords(self):
        return self.coordinates


# add platforms class
class PlatformSprite(Sprite):
    def __init__(self, game, photo_image, x, y, width, height):
        Sprite.__init__(self, game)
        self.photo_image = photo_image
        self.image = game.canvas.create_image(x, y
                                              , image=self.photo_image, anchor='nw')
        self.coordinates = Coords(x, y, x + width, y + height)


class Game:
    def __init__(self):
        self.tk = Tk()
        self.tk.title("Mr.Stickan races for the exit!")
        self.tk.resizable(0, 0)
        self.tk.wm_attributes("-topmost", 1)
        self.canvas = Canvas(self.tk, width=500, height=500, highlightthickness=0)
        self.canvas.pack()
        self.tk.update()
        self.canvas_height = 500
        self.canvas_width = 500
        self.bg = PhotoImage(file='~/python/stickman/backgrond.gif')
        w = self.bg.width()
        h = self.bg.height()
        for x in range(0, 5):
            for y in range(0, 5):
                self.canvas.create_image(x * w, y * h, image=self.bg, anchor='nw')
        self.sprites = []
        self.running = True
        print("I'm lonely!")

    def mainloop(self):
        while 1:
            if self.running == True:
                for sprite in self.sprites:
                    sprite.move()
            self.tk.update_idletasks()
            self.tk.update()
            time.sleep(0.01)
            # print("I'm lonely!")


g = Game()
platform1 = PlatformSprite(g, PhotoImage(file="platform1.gif") \
                           , 0, 480, 100, 10)
platform2 = PlatformSprite(g, PhotoImage(file="platform1.gif") \
                           , 150, 440, 100, 10)
platform3 = PlatformSprite(g, PhotoImage(file="platform1.gif") \
                           , 300, 400, 100, 10)
platform4 = PlatformSprite(g, PhotoImage(file="platform1.gif") \
                           , 300, 160, 100, 10)
platform5 = PlatformSprite(g, PhotoImage(file="platform2.gif") \
                           , 175, 350, 66, 10)
platform6 = PlatformSprite(g, PhotoImage(file="platform2.gif") \
                           , 50, 300, 66, 10)
platform7 = PlatformSprite(g, PhotoImage(file="platform2.gif") \
                           , 170, 120, 66, 10)
platform8 = PlatformSprite(g, PhotoImage(file="platform2.gif") \
                           , 45, 60, 66, 10)
platform9 = PlatformSprite(g, PhotoImage(file="platform3.gif") \
                           , 170, 250, 32, 10)
platform10 = PlatformSprite(g, PhotoImage(file="platform3.gif") \
                            , 230, 200, 32, 10)
g.sprites.append(platform1)
g.sprites.append(platform2)
g.sprites.append(platform3)
g.sprites.append(platform5)
g.sprites.append(platform6)
g.sprites.append(platform7)
g.sprites.append(platform8)
g.sprites.append(platform9)
g.sprites.append(platform10)
g.mainloop()
